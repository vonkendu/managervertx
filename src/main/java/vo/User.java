package vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.vertx.core.json.JsonObject;

public class User {

    public static final String ID = "_id";
    public static final String PASSWORD = "password";
    public static final String USERNAME = "username";
    public static final String USER_ROLE = "role";
    @JsonProperty(ID)
    private final String id;

    @JsonProperty(PASSWORD)
    private final String password;

    @JsonProperty(USERNAME)
    private final String username;

    @JsonProperty(USER_ROLE)
    private final String userRole;

    public User(JsonObject json) {
        this.id = json.getString(ID);
        this.password = json.getString(PASSWORD);
        this.username = json.getString(USERNAME);
        this.userRole = json.getString(USER_ROLE);

    }
}
