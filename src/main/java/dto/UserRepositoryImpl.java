package dto;

import java.util.List;
import java.util.Optional;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import mongo.MongoConnecter;
import mongo.MongoConnecterImpl;

public class UserRepositoryImpl implements UsersRepository {
    private final String entityName = "users";


    @Override public Single<Boolean> exists(JsonObject query) {

        return null;
    }
    @Override public Single<Optional<JsonObject>> findOneById(String id) {
        return null;
    }
    @Override public Single<Optional<JsonObject>> findOneByFields(JsonObject query, JsonObject fields) {
        return null;
    }
    @Override public Single<List<JsonObject>> findAllByFields(JsonObject query) {
        return null;
    }
    @Override public Single<Void> update(String id, JsonObject fieldsToUpdate) {
        return null;
    }
    @Override public Single<String> insert(JsonObject request) {
        return null;
    }
    @Override public Single<Void> remove(JsonObject query) {
        return null;
    }
    @Override public Single<Optional<JsonObject>> findOne(JsonObject query) {
        return null;
    }
}
