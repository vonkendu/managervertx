package dto;

import java.util.List;
import java.util.Optional;

import io.vertx.core.json.JsonObject;
import io.reactivex.Single;

public interface UsersRepository {

    Single<Boolean> exists(JsonObject query);
    Single<Optional<JsonObject>> findOneById(String id);
    Single<Optional<JsonObject>> findOneByFields(JsonObject query, final JsonObject fields);
    Single<List<JsonObject>> findAllByFields(JsonObject query);
    Single<Void> update(String id, JsonObject fieldsToUpdate);
    Single<String> insert(JsonObject request);
    Single<Void> remove(JsonObject query);
    Single<Optional<JsonObject>> findOne(JsonObject query);
}
