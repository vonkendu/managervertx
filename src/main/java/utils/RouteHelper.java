package utils;

import static io.netty.handler.codec.http.HttpHeaderValues.APPLICATION_JSON;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.web.RoutingContext;

public class RouteHelper {
    public static final String STATUS_FIELD = "status";

    public static void generateError(RoutingContext routingContext) {
        routingContext.response().setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code());
        routingContext.response().putHeader(CONTENT_TYPE, APPLICATION_JSON);
        JsonObject response = new JsonObject();
        response.put(STATUS_FIELD, "server error");
        routingContext.response().end(response.encode());
    }

}
