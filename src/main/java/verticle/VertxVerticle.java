package verticle;


import handlers.GetUserHandler;
import handlers.HttpEndpoint;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.reactivex.ext.web.Route;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.handler.CorsHandler;




public class VertxVerticle extends AbstractVerticle {
    private GetUserHandler userHandler = new GetUserHandler();

    @Override
    public void start() throws Exception {
        final Router router = Router.router(vertx);
        registerHandlers(router);


        router.get("/letsSee").handler(userHandler);

        vertx.createHttpServer()
                .requestHandler(router)
                .rxListen(6586)
                .subscribe(
                        server -> System.out.println("Server started."),
                        ex -> System.out.println(ex)
                );;


    }
    @Override
    public void stop() throws Exception {
        System.out.println("BasicVerticle stopped");
    }

    private void registerHandlers(Router router) {

        router.route()
                .handler(CorsHandler.create(".*")
                        .allowedMethod(HttpMethod.GET)
                        .allowedMethod(HttpMethod.POST)
                        .allowedMethod(HttpMethod.DELETE)
                        .allowedMethod(HttpMethod.PATCH)
                        .allowedMethod(HttpMethod.OPTIONS)
                        .allowCredentials(true)
                        .allowedHeader("Access-Control-Allow-Method")
                        .allowedHeader("Access-Control-Allow-Origin")
                        .allowedHeader("Access-Control-Allow-Credentials")
                        .allowedHeader("Content-Type")
                        .allowedHeader("Origin")
                        .allowedHeader("Authorization"));

    }

    private void routeGetHandlers() {

    }


}
