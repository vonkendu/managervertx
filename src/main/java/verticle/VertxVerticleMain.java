package verticle;

import io.vertx.core.Vertx;

public class VertxVerticleMain {
    public static void main(String[] args) throws InterruptedException {
        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(new VertxVerticle());
    }
}
