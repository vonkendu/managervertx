package services;

import io.reactivex.Single;
import io.vertx.core.json.JsonObject;

public interface UserService {
    Single<JsonObject> getUser(String user);
}
