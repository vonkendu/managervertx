package mongo;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

public class MongoConnecterImpl implements MongoConnecter {
    MongoClient mongoClient;
    public MongoConnecterImpl(final Vertx vertx, final JsonObject config) {
        this.mongoClient = MongoClient.createShared(vertx, config);
    }
}
