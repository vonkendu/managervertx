package handlers;

import io.reactivex.Single;
import io.vertx.reactivex.ext.web.RoutingContext;
import utils.RouteHelper;

public abstract class SecuredHandler<T> implements RxHandler{
    @Override
    public void handle(final RoutingContext routingContext) {
       // Here will be security check
        Single.just(routingContext)
                .flatMap(context -> process(context))
                .subscribe(
                        result -> generateResponse(routingContext, result),
                        ex -> RouteHelper.generateError(routingContext)
                );
    }
    @Override public String getConsumes() {
        return "ALL";
    }
    @Override public String getProduces() {
        return "APPLICATION_JSON";
    }

    abstract Single<T> process(RoutingContext routingContext);

    abstract void generateResponse(final RoutingContext routingContext, T result);


}
