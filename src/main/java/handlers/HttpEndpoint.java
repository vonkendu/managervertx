package handlers;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;

public interface HttpEndpoint<H> extends Handler<H> {

    String CONTENT_TYPE_HEADER_NAME = "Content-Type";

    HttpMethod getMethod();

    String getUrlEndpoint();

    String getConsumes();

    String getProduces();

    default void setContentTypeAsProduces(RoutingContext routingContext) {

        routingContext.response().putHeader(CONTENT_TYPE_HEADER_NAME, getProduces());
    }
}
