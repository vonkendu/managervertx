package handlers;


import io.netty.handler.codec.http.HttpResponseStatus;
import io.reactivex.*;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.core.json.JsonObject;
import services.UserService;
import services.UserServiceImpl;

public class GetUserHandler extends SecuredHandler<JsonObject>  {
    UserService userService = new UserServiceImpl();

    @Override

    public HttpMethod getMethod() {
        return HttpMethod.GET;
    }
    @Override
    public String getUrlEndpoint()
    {
        return "/getUser";
    }


    @Override
    public Single<JsonObject> process(RoutingContext routingContext) {

    return Single.just(routingContext)
            .flatMap(userString -> userService.getUser("Sasd"));

    }


    @Override
    void generateResponse(RoutingContext routingContext, JsonObject result) {

        routingContext.response().putHeader("Content-Type", "application/json");
        routingContext.response().setStatusCode(HttpResponseStatus.OK.code());
        routingContext.response().end(result.encode());
    }


}
