package handlers;


import io.vertx.reactivex.ext.web.RoutingContext;

public interface  RxHandler extends HttpEndpoint<RoutingContext> {

    @Override
    void handle(RoutingContext routingContext);
}
